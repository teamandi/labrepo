package catalog;

import service.ProblemService;
import service.StudentService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {
    private StudentService studentService;
    private ProblemService problemService;

    public Console(StudentService studentService, ProblemService problemService) {
        this.studentService = studentService;
        this.problemService = problemService;
    }
    public void runConsole() {
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter a command: ");
            try {
                cmd = bufferRead.readLine();
                switch (cmd) {
                    case "1":
                        StudentCRUDConsole studentCRUDConsole = new StudentCRUDConsole(studentService);
                        studentCRUDConsole.runConsole();
                        break;
                    case "2":
                        ProblemCRUDConsole problemCRUDConsole = new ProblemCRUDConsole(problemService);
                        problemCRUDConsole.runConsole();
                        break;
                    case "3":
                        AssigningConsole assigningConsole = new AssigningConsole(studentService, problemService);
                        assigningConsole.runConsole();
                        break;
                    case "0":
                        //bufferRead.close();
                        ok=false;
                    default:
                        break;
                }
            }
            catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
    }

    private void menu(){
        System.out.println("1. Student CRUD.");
        System.out.println("2. Problems CRUD");
        System.out.println("3. Assign Console");
        System.out.println("0. Exit.");
    }
}
