package catalog;

import domain.Problem;
import domain.Student;
import domain.validators.ProblemValidator;
import domain.validators.StudentValidator;
import domain.validators.Validator;
import repository.CSV.ProblemCSVRepo;
import repository.CSV.StudentCSVRepo;
import repository.InMemoryRepo;
import repository.Repository;
import repository.SQL.ProblemSQLRepo;
import repository.SQL.StudentSQLRepo;
import repository.XML.ProblemXMLRepo;
import repository.XML.StudentXMLRepo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RepoTypeConsole {

    private Validator<Student> studentValidator;
    private Validator<Problem> problemValidator;
    private Repository<Long,Problem> problemRepository;
    private Repository<Long,Student> studentRepository;

    public RepoTypeConsole(){
        studentValidator = new StudentValidator();
        problemValidator = new ProblemValidator();
    }

    public Repository[] runConsole() {
        String cmd;
        menu();
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Select repo type: ");
        try {
            cmd = bufferRead.readLine();
            switch (cmd) {
                case "1":
                    problemRepository = new InMemoryRepo<>(problemValidator);
                    studentRepository = new InMemoryRepo<>(studentValidator);
                    break;
                case "2":
                    problemRepository = new ProblemXMLRepo(problemValidator, "./data/problems.xml");
                    studentRepository = new StudentXMLRepo(studentValidator, "./data/students.xml");
                    break;
                case "3":
                    problemRepository = new ProblemCSVRepo(problemValidator, "./data/problems.csv");
                    studentRepository = new StudentCSVRepo(studentValidator, "./data/students.csv");
                    break;
                case "4":
                    String url = "jdbc:postgresql://baasu.db.elephantsql.com:5432/pkzccpxq";
                    String username = "pkzccpxq";
                    String password = "B357W-Hxfjabf4gquK_ZbfPBVZvD0MLk";
                    problemRepository = new ProblemSQLRepo(problemValidator, url, username, password);
                    studentRepository = new StudentSQLRepo(studentValidator, url, username, password);
                    break;
                default:
                    problemRepository = new InMemoryRepo<>(problemValidator);
                    studentRepository = new InMemoryRepo<>(studentValidator);
                    break;
            }
            return new Repository[]{studentRepository, problemRepository};
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    private void menu(){
        System.out.println("1. In memory.");
        System.out.println("2. XML.");
        System.out.println("3. CSV");
        System.out.println("4. SQL");
    }
}
