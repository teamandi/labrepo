package catalog;

import domain.Student;
import domain.validators.ValidatorException;
import service.StudentService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.Set;

public class StudentCRUDConsole {
    private StudentService studentService;

    public StudentCRUDConsole(StudentService studentService) {
        this.studentService = studentService;
    }

    public void runConsole() {
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter a command: ");
            try {
                cmd = bufferRead.readLine();
                switch (cmd) {
                    case "1":
                        addStudents();
                        System.out.println("Student added successfully!");
                        break;
                    case "2":
                        printAllStudents();
                        break;
                    case "3":
                        updateStudent();
                        break;
                    case "4":
                        deleteStudent();
                        System.out.println("Student was deleted!");
                        break;
                    case "0":
                        ok=false;
                        break;
                    default:
                        break;
                }
            }
            catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
    }


    private void printAllStudents() {
        Set<Student> students = studentService.getAllStudents();
        students.stream().forEach(System.out::println);
    }

    private void menu(){
        System.out.println("1. Read a student.");
        System.out.println("2. Print all students.");
        System.out.println("3. Update a student");
        System.out.println("4. Delete a student");
        System.out.println("0. Exit.");
    }

    private void addStudents() {
        Student student = readStudent();
        try {
            if(student!=null)
                studentService.addStudent(student);
        } catch (ValidatorException e) {
            System.out.println(e.getMessage());
        }
    }

    private void updateStudent(){
        System.out.println("Update student. Please specify the ID of the student you want to update");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferedReader.readLine());
            Student s = studentService.getStudent(id).orElseThrow(() -> new Exception("No student has that ID could not update"));
            System.out.println("Now specify the fields you want to change (leave empty, if you don't want to modify)");

            System.out.print("Serial Number = ");
            s.setSerialNumber(Optional.ofNullable(bufferedReader.readLine()).filter(str->!str.isEmpty()).orElse(s.getSerialNumber()));
            System.out.print("Name = ");
            s.setName(Optional.ofNullable(bufferedReader.readLine()).filter(str->!str.isEmpty()).orElse(s.getName()));
            System.out.print("Group = ");
            s.setGroup(Integer.parseInt(Optional.ofNullable(bufferedReader.readLine()).orElse(String.valueOf(s.getGroup()))));

            //The student is validated inside the update function
            studentService.updateStudent(s);
        }
        catch (IOException e) {
            System.out.println("Sorry, the ID should be a number");
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void deleteStudent(){
        System.out.println("Delete student. What is the ID of the student you want to delete?");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferedReader.readLine());
            studentService.deleteStudent(id);//.orElseThrow(()->new Exception("No student has that ID. Could not delete"));
        }
        catch (IOException e) {
            System.out.println("Sorry, the ID should be a number");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private Student readStudent() {
        System.out.println("Read student {id,serialNumber, name, group}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferRead.readLine());
            System.out.print("Serial Number = ");
            String serialNumber = bufferRead.readLine();
            System.out.print("Name = ");
            String name = bufferRead.readLine();
            System.out.print("Group = ");
            int group = Integer.parseInt(bufferRead.readLine());
            Student student = new Student(serialNumber, name, group);
            student.setId(id);

            return student;
        }
        catch (NumberFormatException nfe){
            System.out.println(nfe.getMessage());
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}