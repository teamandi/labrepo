package catalog;

import domain.Problem;
import domain.Student;
import service.ProblemService;
import service.StudentService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AssigningConsole {
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RESET = "\u001B[0m";
    private StudentService studentService;
    private ProblemService problemService;

    public AssigningConsole(StudentService studentService, ProblemService problemService) {
        this.studentService = studentService;
        this.problemService = problemService;
    }

    public void runConsole(){
        String cmd;
        boolean ok=true;
        while(ok) {
            menu();
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter a command: ");
            try {
                cmd = bufferRead.readLine();
                switch (cmd) {
                    case "1":
                        printAll();
                        break;
                    case "2":
                        assignProblem();
                        break;
                    case "3":
                        gradeStudent();
                        break;
                    case "4":
                        deleteProblem();
                        break;
                    case "0":
                        ok = false;
                        break;
                    default:
                        System.out.println("Invalid command. Please try again");
                        break;
                }
            }
            catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
    }

    private void menu(){
        System.out.println("1. See students and assigned problems");
        System.out.println("2. Assign a problem to a student");
        System.out.println("3. Grade a student");
        System.out.println("4. Remove a problem from a student");
        System.out.println("0. Go back");
    }

    private void printAll() {
        studentService
                .getAllStudents()
                .stream()
                .forEach(
                        s -> {
                            System.out.println(ANSI_GREEN + "<" + s + "> has assigned the following problems:" + ANSI_RESET);
                            s.getProblems().stream().forEach(p -> System.out.println(ANSI_RED + "\t" + problemService.getProblem(p) + ANSI_RESET));
                        }
                        );
    }

    private void assignProblem() {
        System.out.println("Please provide the ID of the student you want to assign problems to");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferedReader.readLine());
            Student s = studentService.getStudent(id).orElseThrow(() -> new Exception("No student has that ID"));
            System.out.println("Now provide the ID of the problem you want to assign to <" + s + ">");
            System.out.print("ID = ");
            id = Long.valueOf(bufferedReader.readLine());
            Problem p = problemService.getProblem(id).orElseThrow(() -> new Exception("No problem has that ID"));
            s.addProblem(id);
            studentService.updateStudent(s);
            System.out.println(ANSI_GREEN + p
                    + ANSI_RED + " -- successfully assigned to -- "
                    + ANSI_GREEN + s + ANSI_RESET);
        }
        catch (IOException e){
            System.out.println("Sorry, ID should be a number");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void gradeStudent() {
        System.out.println("Please provide the ID of the student you want to grade");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferedReader.readLine());
            Student s = studentService.getStudent(id).orElseThrow(() -> new Exception("No student has that ID"));
            System.out.println("Now provide the ID of the problem you want to grade");
            id = Long.valueOf(bufferedReader.readLine());
            s.getProblem(id).orElseThrow(() -> new Exception("Student has not been assigned that problem"));
            System.out.println("What grade would you like to assign to this problem?");
            float grade = Float.valueOf(bufferedReader.readLine());
            s.gradeProblem(id, grade);
            studentService.updateStudent(s);
        }
        catch (IOException e){
            System.out.println("Sorry, ID should be a number");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void deleteProblem(){
        System.out.println("Please provide the ID of the student you want to delete a problem from");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferedReader.readLine());
            Student s = studentService.getStudent(id).orElseThrow(() -> new Exception("No student has that ID"));
            System.out.println("Now provide the ID of the problem you want to remove");
            id = Long.valueOf(bufferedReader.readLine());
            s.getProblem(id).orElseThrow(() -> new Exception("Student has not been assigned that problem"));
            s.removeProblem(id);
            studentService.updateStudent(s);
            System.out.println(ANSI_RED + "Problem successfully removed" + ANSI_RESET);
        }
        catch (IOException e){
            System.out.println("Sorry, ID should be a number");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
