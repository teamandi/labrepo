package catalog;

import domain.Problem;
import domain.validators.ValidatorException;
import service.ProblemService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;

public class ProblemCRUDConsole {
    private ProblemService problemService;

    public ProblemCRUDConsole(ProblemService problemService) {
        this.problemService = problemService;
    }

    public void runConsole() {
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter a command: ");
            try {
                cmd = bufferRead.readLine();
                switch (cmd) {
                    case "1":
                        addProblem();
                        break;
                    case "2":
                        printAllProblems();
                        break;
                    case "3":
                        updateProblem();
                        break;
                    case "4":
                        deleteProblem();
                        break;
                    case "0":
                        ok=false;
                        break;
                    default:
                        break;
                }
            }
            catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
    }


    private void printAllProblems() {
        Set<Problem> problems = problemService.getAllProblems();
        problems.stream().forEach(System.out::println);
    }

    private void menu(){
        System.out.println("1. Read a problem.");
        System.out.println("2. Print all problems.");
        System.out.println("3. Update a problem.");
        System.out.println("4. Delete a problem.");
        System.out.println("0. Exit.");
    }

    private void updateProblem(){
        try {
            Problem problem = readProblem();
            problemService.updateProblem(problem);
        }
        catch(ValidatorException ex){
            System.out.println(ex.getMessage());
        }
    }

    private void deleteProblem(){
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferRead.readLine());
            problemService.removeProblem(id);
        }
        catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void addProblem() {
        Problem problem = readProblem();
        try {
            if(problem!=null)
                problemService.addProblem(problem);
        } catch (ValidatorException e) {
            System.out.println(e.getMessage());
        }
    }

    private Problem readProblem() {
        System.out.println("Read problem {id, problemDescription, grade}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferRead.readLine());
            System.out.print("Problem Description = ");
            String problemDescription = bufferRead.readLine();


            Problem problem = new Problem(problemDescription);
            problem.setId(id);

            return problem;
        }
        catch (NumberFormatException nfe){
            System.out.println(nfe.getMessage());
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
