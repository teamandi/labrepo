package service;

import domain.Problem;
import domain.validators.ValidatorException;
import repository.Repository;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ProblemService {
    private Repository<Long, Problem> problemRepository;

    public ProblemService(Repository<Long, Problem> problemRepository) {
        this.problemRepository = problemRepository;
    }

    public void addProblem(Problem problem) throws ValidatorException {
        problemRepository.save(problem);
    }

    public void removeProblem(Long id) throws IllegalArgumentException{
        problemRepository.delete(id);
    }

    public void updateProblem(Problem problem)throws IllegalArgumentException, ValidatorException{
        problemRepository.update(problem);
    }

    public Optional<Problem> getProblem(Long id){
        return problemRepository.findOne(id);
    }

    public Set<Problem> getAllProblems() {
        Iterable<Problem> problems = problemRepository.findAll();
        return StreamSupport.stream(problems.spliterator(), false).collect(Collectors.toSet());
    }
}
