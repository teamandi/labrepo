package domain.validators;

import domain.Student;

/**
 * @author teamandi
 */
public class StudentValidator implements Validator<Student> {

    public void exceptionVerifier(Boolean condition, String message) throws ValidatorException{
        if(condition)
            throw new ValidatorException(message);
    }

    @Override
    public void validate(Student entity) throws ValidatorException {
        exceptionVerifier(entity.getId()<0,"Id under 0.");
        exceptionVerifier(entity.getName().length()<3,"Name can't be so short.");
        exceptionVerifier(entity.getSerialNumber().length()<3,"Serial number too short.");
    }
}
