package domain;

public class Problem extends BaseEntity<Long>{
    private String problemDescription;

    public Problem(String problemDescription) {
        this.problemDescription = problemDescription;
    }

    public Problem(Long id, String problemDescription) {
        super.setId(id);
        this.problemDescription = problemDescription;
    }

    public String getProblemDescription() { return problemDescription; }

    public void setProblemDescription(String problemDescription) {
        this.problemDescription = problemDescription;
    }

    @Override
    public String toString() {
        return "Problem{ id=" + super.getId() +
                ", problemDescription='" + problemDescription + '\'' +
                '}';
    }
}
