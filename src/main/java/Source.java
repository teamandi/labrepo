import catalog.Console;
import catalog.RepoTypeConsole;
import repository.Repository;
import service.ProblemService;
import service.StudentService;

public class Source {
    public static void main(String[] args) {
        RepoTypeConsole repoTypeConsole = new RepoTypeConsole();
        Repository[] repos = repoTypeConsole.runConsole();

        StudentService studentService = new StudentService(repos[0]);
        ProblemService problemService = new ProblemService(repos[1]);


        Console console = new Console(studentService, problemService);
        console.runConsole();
    }
}
