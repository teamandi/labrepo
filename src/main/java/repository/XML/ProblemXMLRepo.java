package repository.XML;

import domain.Problem;
import domain.validators.Validator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ProblemXMLRepo extends XMLRepo<Long, Problem> {

    public ProblemXMLRepo(Validator validator, String fileName) {
        super(validator,fileName);
        try {
            super.entities.putAll(loadFromXML());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public Map<Long, Problem> loadFromXML() throws Exception {
        Map<Long, Problem> problemMap = new HashMap<>();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
        Document xmlDoc = dbBuilder.parse(fileName);
        Element root = xmlDoc.getDocumentElement();
        NodeList childNodes = root.getChildNodes();

        for (int index = 0; index < childNodes.getLength(); index++) {
            Node child = childNodes.item(index);
            if (child instanceof Element) {
                Element problem = (Element) child;
                String category = problem.getAttribute("problem");

                String id = getTextByTagName(problem, "id");
                String problemdescription = getTextByTagName(problem, "problemdescription");

                Problem newProblem = new Problem(problemdescription);
                newProblem.setId(Long.valueOf(id));
                problemMap.put(newProblem.getId(), newProblem);
            }
        }

        return problemMap;
    }

    public void saveToXML(Problem pb) throws Exception {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
        Document xmlDoc = dbBuilder.parse(new File(fileName));
        Element root = xmlDoc.getDocumentElement();

        Element ourProblem = xmlDoc.createElement("problem");
        appendChildToElement(xmlDoc, ourProblem, "id", pb.getId().toString());
        appendChildToElement(xmlDoc, ourProblem, "problemdescription", pb.getProblemDescription());
        root.appendChild(ourProblem);


        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        //transformerFactory.setAttribute("indent-number", new Integer(2));
        Transformer transformer = transformerFactory.newTransformer();
        //transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(root);
        StreamResult result = new StreamResult(fileName);

        // Output to console for testing
        // StreamResult result = new StreamResult(System.out);

        transformer.transform(source, result);
    }
}