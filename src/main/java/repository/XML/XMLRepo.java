package repository.XML;


import domain.BaseEntity;
import domain.validators.Validator;
import domain.validators.ValidatorException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import repository.InMemoryRepo;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.util.Map;
import java.util.Optional;

public abstract class XMLRepo <ID, T extends BaseEntity<ID>> extends InMemoryRepo<ID,T> {

    protected String fileName;

    public XMLRepo(Validator validator,String fileName){
        super(validator);
        this.fileName=fileName;
    }

    protected String getTextByTagName(Element element, String tag){
        return element.getElementsByTagName(tag).item(0).getTextContent();
    }

    protected void appendChildToElement(Document doc, Element parent, String tag, String value){
        Element element = doc.createElement(tag);
        element.setTextContent(value);
        parent.appendChild(element);
    }

    public abstract Map<ID,T> loadFromXML() throws Exception;

    public abstract void saveToXML(T e) throws Exception;

    public void deleteFromXML(ID id) throws Exception{
        DocumentBuilderFactory dbFactory =  DocumentBuilderFactory.newInstance();
        DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
        Document xmlDoc = dbBuilder.parse(fileName);
        Element root = xmlDoc.getDocumentElement();
        NodeList childNodes = root.getChildNodes();

        for (int index = 0; index < childNodes.getLength(); index++){
            Node child = childNodes.item(index);
            if(child instanceof Element) {
                Element student = (Element) child;
                String searchid = getTextByTagName(student, "id");
                if (Long.valueOf(searchid)==id){
                    root.removeChild(child);
                }
            }
        }

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer();
        t.transform(new DOMSource(xmlDoc), new StreamResult(fileName));
    }

    public void updateFromXML(T e) throws Exception{
        deleteFromXML(e.getId());
        saveToXML(e);
    }

    @Override
    public Optional<T> save(T entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("id must not be null");
        }
        super.save(entity);
        try {
            saveToXML(entity);
        }catch(Exception e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<T> delete(ID id){
        if (id == null) {
            throw new IllegalArgumentException("id must not be null");
        }
        super.delete(id);
        try {
            deleteFromXML(id);
        }catch(Exception e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<T> update(T entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("entity must not be null");
        }
        super.update(entity);
        try {
            updateFromXML(entity);
        }catch(Exception e){
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
