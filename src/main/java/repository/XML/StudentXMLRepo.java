package repository.XML;

import domain.Student;
import domain.validators.Validator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


public class StudentXMLRepo extends XMLRepo<Long,Student> {


    public StudentXMLRepo(Validator validator,String fileName){
        super(validator,fileName);
        try {
            super.entities.putAll(loadFromXML());
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public Map<Long, Student> loadFromXML() throws Exception {
        Map<Long,Student> studentMap = new HashMap<>();
        DocumentBuilderFactory dbFactory =  DocumentBuilderFactory.newInstance();
        DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
        Document xmlDoc = dbBuilder.parse(super.fileName);
        Element root = xmlDoc.getDocumentElement();
        NodeList childNodes = root.getChildNodes();

        for (int index = 0; index < childNodes.getLength(); index++){
            Node child = childNodes.item(index);
            if(child instanceof Element) {
                Element student = (Element) child;
                String category = student.getAttribute("problem");

                String id = getTextByTagName(student, "id");
                String serialnumber = getTextByTagName(student, "serialnumber");
                String name = getTextByTagName(student, "name");
                String group = getTextByTagName(student, "group");
                String problems = getTextByTagName(student,"problems");
                String[] problemsArray = problems.split(";");


                Student newstudent = new Student(serialnumber, name, Integer.parseInt(group));
                Arrays.stream(problemsArray).forEach(e->{
                    e = e.replace("(","");
                    e = e.replace(")","");
                    String[] problem = e.split(",");
                    newstudent.addProblem(Long.parseLong(problem[0]),Float.parseFloat(problem[1]));
                });
                newstudent.setId(Long.valueOf(id));
                studentMap.put(newstudent.getId(),newstudent);
            }
        }

        return studentMap;
    }

    @Override
    public void saveToXML(Student st) throws Exception {
        DocumentBuilderFactory dbFactory =  DocumentBuilderFactory.newInstance();
        DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
        Document xmlDoc = dbBuilder.parse(new File(fileName));
        Element root = xmlDoc.getDocumentElement();

        Element ourStudent = xmlDoc.createElement("student");
        appendChildToElement(xmlDoc, ourStudent, "id", st.getId().toString());
        appendChildToElement(xmlDoc, ourStudent, "name", st.getName());
        appendChildToElement(xmlDoc, ourStudent, "serialnumber", st.getSerialNumber());
        appendChildToElement(xmlDoc, ourStudent, "group", String.valueOf(st.getGroup()));
        String problems=st.getProblems().stream().map(e->"("+e+","+st.getGrade(e)+")").collect(Collectors.joining(";",";",""));
        appendChildToElement(xmlDoc,ourStudent,"problems",problems);
        root.appendChild(ourStudent);


        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        //transformerFactory.setAttribute("indent-number", new Integer(2));
        Transformer transformer = transformerFactory.newTransformer();
        //transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(root);
        StreamResult result = new StreamResult(fileName);

        // Output to console for testing
        // StreamResult result = new StreamResult(System.out);

        transformer.transform(source, result);
    }
}
