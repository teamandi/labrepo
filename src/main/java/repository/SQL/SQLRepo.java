package repository.SQL;

import domain.BaseEntity;
import domain.validators.Validator;
import domain.validators.ValidatorException;
import repository.InMemoryRepo;

import java.util.Map;
import java.util.Optional;

public abstract class SQLRepo <ID, T extends BaseEntity<ID>> extends InMemoryRepo<ID,T> {
    protected String url;
    protected String username;
    protected String password;

    public SQLRepo(Validator validator, String url,String username,String password){
        super(validator);
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public abstract Map<ID,T> loadFromSQL() throws Exception;

    public abstract void saveToSQL(T e) throws Exception;

    public abstract void deleteFromSQL(ID id) throws Exception;

    public void updateFromSQL(T e) throws Exception{
        deleteFromSQL(e.getId());
        saveToSQL(e);
    }

    @Override
    public Optional<T> save(T entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("id must not be null");
        }
        super.save(entity);
        try {
            saveToSQL(entity);
        }catch(Exception e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<T> delete(ID id){
        if (id == null) {
            throw new IllegalArgumentException("id must not be null");
        }
        super.delete(id);
        try {
            deleteFromSQL(id);
        }catch(Exception e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<T> update(T entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("entity must not be null");
        }
        super.update(entity);
        try {
            updateFromSQL(entity);
        }catch(Exception e){
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
