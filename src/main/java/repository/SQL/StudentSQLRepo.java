package repository.SQL;

import domain.Student;
import domain.validators.Validator;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class StudentSQLRepo extends SQLRepo<Long, Student> {

    public StudentSQLRepo(Validator validator, String url, String username, String password){
        super(validator,url,username,password);
        try {
            super.entities.putAll(loadFromSQL());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public Map<Long, Student> loadFromSQL() throws Exception {
        Map<Long, Student> students = new HashMap<>();
        Connection db = DriverManager.getConnection(super.url, super.username, super.password);
        Statement newST = db.createStatement();
        ResultSet rs = newST.executeQuery("SELECT * FROM Students;");
        Statement pbST = null;
        ResultSet pbrs = null;
        String serialnumber,name;
        Long id;
        int group,problemsID;
        long pbID;
        float grade;
        while (rs.next()) {
            id = Long.valueOf(rs.getString(1));
            serialnumber = rs.getString(2);
            name = rs.getString(3);
            group = Integer.valueOf(rs.getString(4));
            problemsID = Integer.valueOf(rs.getString(5));
            Student newStudent = new Student(serialnumber,name,group);
            newStudent.setId(id);
            pbST = db.createStatement();
            pbrs = pbST.executeQuery("SELECT * FROM studentproblems WHERE studentid="+ problemsID);
            while(pbrs.next()){
                pbID = Long.valueOf(pbrs.getString(2));
                grade = Float.valueOf(pbrs.getString(3));
                newStudent.addProblem(pbID,grade);
            }
            students.put(newStudent.getId(), newStudent);
        }
        rs.close();
        newST.close();


        db.close();
        return students;
    }

    @Override
    public void saveToSQL(Student e) throws Exception {
        Connection db = DriverManager.getConnection(url, username, password);
        PreparedStatement st = db.prepareStatement("INSERT INTO Students (ID, serialnumber,name,studentgroup,problems) VALUES (?, ?, ?, ?, ?)");
        PreparedStatement pbST = db.prepareStatement("INSERT INTO StudentProblems (studentid, problemid,grade) VALUES (?, ?, ?)");
        e.getProblems().stream().forEach(pb->
        {
            try {
                pbST.setInt(1, Integer.parseInt(e.getId().toString()));
                pbST.setInt(2,Integer.parseInt(pb.toString()));
                pbST.setFloat(3,e.getGrade(pb));

                pbST.executeUpdate();
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }

        });
        st.setInt(1, Integer.parseInt(e.getId().toString()));
        st.setString(2, e.getSerialNumber());
        st.setString(3,e.getName());
        st.setInt(4,e.getGroup());
        st.setInt(5,Integer.parseInt(e.getId().toString()));

        st.executeUpdate();
        pbST.close();
        st.close();
        db.close();
    }

    @Override
    public void deleteFromSQL(Long aLong) throws Exception {
        Connection db = DriverManager.getConnection(url, username, password);
        PreparedStatement st = db.prepareStatement("DELETE FROM Students WHERE ID = ?");
        st.setInt(1,Integer.parseInt(aLong.toString()));
        st.executeUpdate();
        st = db.prepareStatement("DELETE FROM studentproblems WHERE studentid = ?");
        st.setInt(1,Integer.parseInt(aLong.toString()));
        st.executeUpdate();
        st.close();
        db.close();
    }

}
