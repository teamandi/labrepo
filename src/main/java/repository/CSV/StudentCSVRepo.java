package repository.CSV;

import domain.Student;
import domain.validators.Validator;

import java.util.stream.Collectors;

public class StudentCSVRepo extends CSVRepo<Long, Student>{
    public StudentCSVRepo(Validator<Student> validator, String filename) {
        super(validator, filename);

        super.mapToItem = (line) -> {
            String[] p = line.split(";");
            Student s = new Student(Long.parseLong(p[0]), p[1], p[2], Integer.parseInt(p[3]));
            if(p.length > 4)
                for(int i = 4; i < p.length; i+=2)
                    s.addProblem(Long.parseLong(p[i]), Float.parseFloat(p[i+1]));
            return s;
        };

        super.mapToString = (entry) -> {
            String str =
                    entry.getKey() + ";" +
                    entry.getValue().getSerialNumber() + ";" +
                    entry.getValue().getName() + ";" +
                    entry.getValue().getGroup();

            str += entry.getValue().getProblems().stream()
                    .map(p -> p + ";" + entry.getValue().getProblem(p).get())
                    .collect(Collectors.joining(";",";",""));

            return str;
        };

        super.instantiate();
    }
}
