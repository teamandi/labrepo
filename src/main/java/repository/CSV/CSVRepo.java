package repository.CSV;

import domain.BaseEntity;
import domain.validators.Validator;
import domain.validators.ValidatorException;
import repository.InMemoryRepo;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public class CSVRepo <ID, T extends BaseEntity<ID>> extends InMemoryRepo<ID,T> {
    protected String filename;
    protected Function<String, T> mapToItem;
    protected Function<Map.Entry<ID, T>, String> mapToString;

    public CSVRepo(Validator<T> validator, String filename) {
        super(validator);
        this.filename = filename;
    }

    public void instantiate(){
        try {
            super.entities.putAll(this.loadFromCSV());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Map<ID,T> loadFromCSV() throws Exception{
        Map<ID, T> myEntities = new HashMap<>();
        try {
            File inputFile = new File(filename);
            InputStream inputStream = new FileInputStream(inputFile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            reader.lines()
                    .map(mapToItem)
                    .forEach((e)->{
                        try {
                            validator.validate(e);
                            myEntities.putIfAbsent(e.getId(), e);
                        } catch (ValidatorException e1) {
                            System.out.println(e1.getMessage());
                        }
                    });
            reader.close();
        }
        catch (FileNotFoundException e){
            throw new Exception("Could not find file \"" + filename + "\"");
        }
        catch (IOException e){
            throw new Exception("File may be corrupted");
        }
        return myEntities;
    }

    public void saveToCSV() throws Exception{
        try {
            FileWriter writer = new FileWriter(filename);
            entities.entrySet().stream()
                    .map(mapToString)
                    .forEach((str) ->{
                        try {
                            writer.write(str + "\n");
                        } catch (IOException e) {
                            System.out.println(e.getMessage());
                        }
                    });
            writer.close();
        } catch (FileNotFoundException e) {
            throw new Exception("Could not find file \"" + filename + "\"");
        }
    }

    @Override
    public Optional<T> save(T entity) throws ValidatorException {
        Optional<T> ret = super.save(entity);
        try {
            saveToCSV();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    @Override
    public Optional<T> delete(ID id) {
        Optional<T> ret = super.delete(id);
        try {
            saveToCSV();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;

    }

    @Override
    public Optional<T> update(T entity) throws ValidatorException {
        Optional<T> ret = super.update(entity);
        try {
            saveToCSV();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }
}
