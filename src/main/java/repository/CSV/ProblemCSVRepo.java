package repository.CSV;

import domain.Problem;
import domain.validators.Validator;

public class ProblemCSVRepo extends CSVRepo<Long, Problem> {
    public ProblemCSVRepo(Validator<Problem> validator, String filename) {
        super(validator, filename);

        super.mapToItem = (line) -> {
            String[] p = line.split(";");
            return new Problem(Long.parseLong(p[0]), p[1]);
        };

        super.mapToString = (entry) -> {
            return entry.getKey() + ";" +
                    entry.getValue().getProblemDescription();
        };

        super.instantiate();
    }
}
