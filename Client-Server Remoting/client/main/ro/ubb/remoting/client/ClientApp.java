package ro.ubb.remoting.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ro.ubb.remoting.client.ui.Console;
import ro.ubb.remoting.common.Service;

public class ClientApp {
    public static void main(String[] args){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("ro.ubb.remoting.client.config");
        Service student = (Service) context.getBean("ServiceClient");

        Console console = new Console(student);
        console.runConsole();

        System.out.println("bye - client");
    }
}
