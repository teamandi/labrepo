package ro.ubb.remoting.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import ro.ubb.remoting.client.service.ServiceImplClient;
import ro.ubb.remoting.common.Service;

@Configuration
public class ClientConfig {
    @Bean
    Service ServiceClient(){return new ServiceImplClient() {
    };}

    @Bean
    RmiProxyFactoryBean rmiProxyFactoryBean(){
        RmiProxyFactoryBean proxy = new RmiProxyFactoryBean();
        proxy.setServiceInterface(Service.class);
        proxy.setServiceUrl("rmi://192.168.43.135:1099/Service");
        return proxy;
    }
}
