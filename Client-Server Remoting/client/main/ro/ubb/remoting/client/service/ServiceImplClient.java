package ro.ubb.remoting.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import ro.ubb.remoting.common.Problem;
import ro.ubb.remoting.common.Service;
import ro.ubb.remoting.common.Student;
import ro.ubb.remoting.common.Tuple;

public class ServiceImplClient implements Service{
    @Autowired
    private Service service;

    @Override
    public Iterable<Student> getStudents() {
        return service.getStudents();
    }

    @Override
    public Student getStudent(Long id) { return service.getStudent(id); }

    @Override
    public String addStudent(Student student) {
        return service.addStudent(student);
    }

    @Override
    public String deleteStudent(Long id) {
        return service.deleteStudent(id);
    }

    @Override
    public String updateStudent(Student student) {
        return service.updateStudent(student);
    }

    @Override
    public Iterable<Problem> getProblems() {
        return service.getProblems();
    }

    @Override
    public Problem getProblem(Long id) {
        return service.getProblem(id);
    }

    @Override
    public String addProblem(Problem problem) {
        return service.addProblem(problem);
    }

    @Override
    public String deleteProblem(Long id) {
        return service.deleteProblem(id);
    }

    @Override
    public String updateProblem(Problem problem) {
        return service.updateProblem(problem);
    }

    @Override
    public Tuple<Iterable<Student>, Iterable<Problem>> getAssignments() {
        return service.getAssignments();
    }

    @Override
    public String assignProblem(Long idStudent, Long idProblem) {
        return null;
    }

    @Override
    public String gradeProblem(Long idStudent, Long idProblem, Float grade) {
        return null;
    }

    @Override
    public String unassignProblem(Long idStudent, Long idProblem) {
        return null;
    }
}
