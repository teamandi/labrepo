package ro.ubb.remoting.client.ui;

import ro.ubb.remoting.common.Problem;
import ro.ubb.remoting.common.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;

public class ProblemCRUDConsole {
    private Service service;
    private Scanner input;
    private ExecutorService executorService;

    public ProblemCRUDConsole(Service service, Scanner scanner, ExecutorService executorService) {
        this.service = service;
        this.input = scanner;
        this.executorService = executorService;
    }

    public void runConsole(){
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            System.out.print("Enter a command: ");
            cmd = input.next();
            switch (cmd) {
                case "1":
                    executorService.submit(()->addProblem());
                    break;
                case "2":
                    executorService.submit(()->printAllProblems());
                    break;
                case "3":
                    executorService.submit(()->updateProblem());
                    break;
                case "4":
                    executorService.submit(()->deleteProblem());
                    break;
                case "0":
                    ok=false;
                    break;
                default:
                    break;
            }
        }
    }

    private void menu(){
        System.out.println("1. Read a problem.");
        System.out.println("2. Print all problems.");
        System.out.println("3. Update a problem.");
        System.out.println("4. Delete a problem.");
        System.out.println("0. Exit.");
    }

    private void printAllProblems() {
        Iterable<Problem> problems = service.getProblems();
        problems.forEach(System.out::println);
    }

    private void addProblem() {
        Problem problem = readProblem();
        System.out.println(service.addProblem(problem));
    }

    private void deleteProblem(){
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("ID = ");
        Long id = null;
        try {
            id = Long.valueOf(bufferRead.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(service.deleteProblem(id));
    }

    private void updateProblem(){
        Problem problem = readProblem();
        System.out.println(service.updateProblem(problem));
    }

    private Problem readProblem() {
        System.out.println("Read problem {id, problemDescription, grade}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferRead.readLine());
            System.out.print("Problem Description = ");
            String problemDescription = bufferRead.readLine();


            Problem problem = new Problem(problemDescription);
            problem.setId(id);

            return problem;
        }
        catch (NumberFormatException nfe){
            System.out.println(nfe.getMessage());
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
