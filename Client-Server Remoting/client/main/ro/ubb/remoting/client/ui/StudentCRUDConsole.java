package ro.ubb.remoting.client.ui;

import ro.ubb.remoting.common.Service;
import ro.ubb.remoting.common.Student;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;

public class StudentCRUDConsole {
    private Service service;
    private Scanner input;
    ExecutorService executorService;

    public StudentCRUDConsole(Service service, Scanner scanner, ExecutorService executorService) {
        this.service = service;
        this.input = scanner;
        this.executorService = executorService;
    }

    public void runConsole(){
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            System.out.print("Enter a command: ");
            cmd = input.next();
            switch (cmd) {
                case "1":
                    executorService.submit(()->this.addStudents());
                    break;
                case "2":
                    executorService.submit(()->this.printAllStudents());
                    break;
                case "3":
                    executorService.submit(()->this.updateStudent());
                    break;
                case "4":
                    executorService.submit(()->this.deleteStudent());
                    break;
                case "0":
                    ok = false;
                    break;
                default:
                    break;
            }
        }
    }

    private void menu(){
        System.out.println("1. Read a student.");
        System.out.println("2. Print all students.");
        System.out.println("3. Update a student");
        System.out.println("4. Delete a student");
        System.out.println("0. Exit.");
    }

    private void printAllStudents() {
        service.getStudents().forEach(System.out::println);
    }

    private void addStudents() {
        System.out.println(service.addStudent(readStudent()));
    }

    private void updateStudent(){
        System.out.println("Update student. Please specify the ID of the student you want to update");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferedReader.readLine());
            Student s = service.getStudent(id);//.orElseThrow(() -> new Exception("No student has that ID could not update"));
            System.out.println("Now specify the fields you want to change (leave empty, if you don't want to modify)");

            System.out.print("Serial Number = ");
            s.setSerialNumber(Optional.ofNullable(bufferedReader.readLine()).filter(str->!str.isEmpty()).orElse(s.getSerialNumber()));
            System.out.print("Name = ");
            s.setName(Optional.ofNullable(bufferedReader.readLine()).filter(str->!str.isEmpty()).orElse(s.getName()));
            System.out.print("Group = ");
            String group = bufferedReader.readLine();
            if (!group.equals(""))
                s.setGroup(Integer.parseInt(group));
            //The student is validated inside the update function
            System.out.println(service.updateStudent(s));
        }
        catch (IOException e) {
            System.out.println("Sorry, the ID should be a number");
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void deleteStudent(){
        System.out.println("Delete student. What is the ID of the student you want to delete?");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferedReader.readLine());
            System.out.println(service.deleteStudent(id));
        }
        catch (IOException e) {
            System.out.println("Sorry, the ID should be a number");
        }
    }

    private Student readStudent() {
        System.out.println("Read student {id,serialNumber, name, group}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferRead.readLine());
            System.out.print("Serial Number = ");
            String serialNumber = bufferRead.readLine();
            System.out.print("Name = ");
            String name = bufferRead.readLine();
            System.out.print("Group = ");
            int group = Integer.parseInt(bufferRead.readLine());
            Student student = new Student(serialNumber, name, group);
            student.setId(id);

            return student;
        }
        catch (NumberFormatException nfe){
            System.out.println(nfe.getMessage());
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
