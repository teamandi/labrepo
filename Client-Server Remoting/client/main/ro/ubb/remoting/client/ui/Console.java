package ro.ubb.remoting.client.ui;

import ro.ubb.remoting.common.Service;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Console {
    private Service service;
    private Scanner input;
    private ExecutorService executorService;

    public Console(Service service) {
        this.service = service;
        this.input = new Scanner(System.in);
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }

    public void runConsole(){
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            System.out.print("Enter a command: ");
            cmd = input.next();
            switch (cmd) {
                case "1":
                    StudentCRUDConsole studentConsole = new StudentCRUDConsole(this.service, this.input, this.executorService);
                    studentConsole.runConsole();
                    break;
                case "2":
                    ProblemCRUDConsole problemConsole = new ProblemCRUDConsole(this.service, this.input, this.executorService);
                    problemConsole.runConsole();
                    break;
                case "3":
                    AssignConsole assignConsole = new AssignConsole(this.service,this.input, this.executorService);
                    assignConsole.runConsole();
                    break;
                case "0":
                    input.close();
                    ok = false;
                    break;
                default:
                    System.out.println("Invalid command. Try again");
                    break;
            }
        }
    }

    private void menu(){
        System.out.println("1. Student CRUD.");
        System.out.println("2. Problems CRUD");
        System.out.println("3. Assign Console");
        System.out.println("0. Exit.");
    }
}
