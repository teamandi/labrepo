package ro.ubb.remoting.common.validators;

public interface Validator<T> {
    void validate(T entity) throws ValidatorException;
}
