package ro.ubb.remoting.common.validators;

import ro.ubb.remoting.common.Problem;

public class ProblemValidator implements Validator<Problem> {

    private void exceptionVerifier(Boolean condition, String message) throws ValidatorException{
        if(condition)
            throw new ValidatorException(message);
    }

    @Override
    public void validate(Problem entity) throws ValidatorException {
        exceptionVerifier(entity.getId()<0,"Id under 0.");
        exceptionVerifier(entity.getProblemDescription().length()<3,"Problem description too short.");
    }
}
