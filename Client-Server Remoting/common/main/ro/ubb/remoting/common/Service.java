package ro.ubb.remoting.common;

public interface Service {
    Iterable<Student> getStudents();
    Student getStudent(Long id);
    String addStudent(Student student);
    String deleteStudent(Long id);
    String updateStudent(Student student);

    Iterable<Problem> getProblems();
    Problem getProblem(Long id);
    String addProblem(Problem problem);
    String deleteProblem(Long id);
    String updateProblem(Problem problem);

    Tuple<Iterable<Student>, Iterable<Problem>> getAssignments();
    String assignProblem(Long idStudent, Long idProblem);
    String gradeProblem(Long idStudent, Long idProblem, Float grade);
    String unassignProblem(Long idStudent, Long idProblem);
}
