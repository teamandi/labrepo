package ro.ubb.remoting.server.service;

import ro.ubb.remoting.common.Problem;
import ro.ubb.remoting.common.Service;
import ro.ubb.remoting.common.Student;
import ro.ubb.remoting.common.Tuple;
import ro.ubb.remoting.common.validators.ValidatorException;
import ro.ubb.remoting.server.repository.Repository;

public class ServiceImpl implements Service {
    Repository<Long, Problem> problemRepo;
    Repository<Long, Student> studentRepo;

    public ServiceImpl(Repository<Long, Problem> problemRepo, Repository<Long, Student> studentRepo) {
        this.problemRepo = problemRepo;
        this.studentRepo = studentRepo;
    }

    @Override
    public Iterable<Student> getStudents() {
        return studentRepo.findAll();
    }

    @Override
    public Student getStudent(Long id){ return studentRepo.findOne(id).get(); }

    @Override
    public String addStudent(Student student) {
        try {
            if(studentRepo.save(student).orElse(null) == null)
                return "Student added successfully";
        } catch (ValidatorException e) {
            return e.getMessage();
        }
        return "meow";
    }

    @Override
    public String deleteStudent(Long id) {
        if (studentRepo.delete(id).orElse(null) == null)
            return "No student has the given ID";
        return "Student deleted successfully";
    }

    @Override
    public String updateStudent(Student student) {
        try {
            if(studentRepo.update(student).orElse(null)== null)
                return "Student updated successfully";
            return "No student has the given ID";
        } catch (ValidatorException e) {
            return e.getMessage();
        }
    }

    @Override
    public Iterable<Problem> getProblems() {
        return problemRepo.findAll();
    }

    @Override
    public Problem getProblem(Long id) {
        return problemRepo.findOne(id).get();
    }

    @Override
    public String addProblem(Problem problem) {
        try {
            if (!problemRepo.save(problem).isPresent())
                return "Problem added successfully";
            return "There already is a problem with the same ID";
        } catch (ValidatorException e) {
            return e.getMessage();
        }
    }

    @Override
    public String deleteProblem(Long id) {
        if (problemRepo.delete(id).isPresent())
            return "Problem deleted successfully";
        return "Could not delete problem";
    }

    @Override
    public String updateProblem(Problem problem) {
        try {
            if(problemRepo.update(problem).isPresent())
                return "Problem updated successfully";
            return "Could not update: invalid ID";
        } catch (ValidatorException e) {
            return e.getMessage();
        }
    }

    @Override
    public Tuple<Iterable<Student>, Iterable<Problem>> getAssignments() {
        Tuple<Iterable<Student>,Iterable<Problem>> tuple = new Tuple<>(studentRepo.findAll(),problemRepo.findAll());
        return tuple;
    }

    @Override
    public String assignProblem(Long idStudent, Long idProblem) {
        //studentRepo.
        return  null;
    }

    @Override
    public String gradeProblem(Long idStudent, Long idProblem, Float grade) {
        return null;
    }

    @Override
    public String unassignProblem(Long idStudent, Long idProblem) {
        return null;
    }
}
