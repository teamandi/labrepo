package ro.ubb.remoting.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import ro.ubb.remoting.common.Service;
import ro.ubb.remoting.common.validators.ProblemValidator;
import ro.ubb.remoting.common.validators.StudentValidator;
import ro.ubb.remoting.server.repository.SQL.ProblemSQLRepo;
import ro.ubb.remoting.server.repository.SQL.StudentSQLRepo;
import ro.ubb.remoting.server.service.ServiceImpl;

@Configuration
public class ServerConfig {
    @Bean
    Service service(){
        return new ServiceImpl(
            new ProblemSQLRepo(
                new ProblemValidator(),
                "jdbc:postgresql://baasu.db.elephantsql.com:5432/pkzccpxq",
                "pkzccpxq",
                "B357W-Hxfjabf4gquK_ZbfPBVZvD0MLk"
            ),
            new StudentSQLRepo(
                new StudentValidator(),
                "jdbc:postgresql://baasu.db.elephantsql.com:5432/pkzccpxq",
                "pkzccpxq",
                "B357W-Hxfjabf4gquK_ZbfPBVZvD0MLk")
        );
    }

    @Bean
    RmiServiceExporter rmiServiceExporter(){
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceName("Service");
        exporter.setServiceInterface(Service.class);
        exporter.setService(this.service());
        return exporter;
    }
}
