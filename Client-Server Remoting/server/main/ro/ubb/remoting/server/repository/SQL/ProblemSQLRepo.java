package ro.ubb.remoting.server.repository.SQL;

import ro.ubb.remoting.common.Problem;
import ro.ubb.remoting.common.validators.Validator;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class ProblemSQLRepo extends SQLRepo<Long, Problem> {

    public ProblemSQLRepo(Validator validator, String url, String username, String password){
        super(validator,url,username,password);
        try {
            super.entities.putAll(loadFromSQL());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public Map<Long, Problem> loadFromSQL() throws Exception {
        Map<Long, Problem> problems = new HashMap<>();
        Connection db = DriverManager.getConnection(super.url, super.username, super.password);
        Statement newST = db.createStatement();
        ResultSet rs = newST.executeQuery("SELECT * FROM Problems;");
        String problemdescription;
        Long id;
        while (rs.next()) {
            id = Long.valueOf(rs.getString(1));
            problemdescription = rs.getString(2);
            Problem newProblem = new Problem(problemdescription);
            newProblem.setId(Long.valueOf(id));
            problems.put(newProblem.getId(), newProblem);
        }
        rs.close();
        newST.close();
        db.close();
        return problems;
    }

    @Override
    public void saveToSQL(Problem e) throws SQLException {
        Connection db = DriverManager.getConnection(url, username, password);
        PreparedStatement st = db.prepareStatement("INSERT INTO Problems (ID, ProblemDescription) VALUES (?, ?)");
        st.setInt(1, Integer.parseInt(e.getId().toString()));
        st.setString(2, e.getProblemDescription());
        st.executeUpdate();
        st.close();
        db.close();
    }

    @Override
    public void deleteFromSQL(Long aLong) throws Exception {
        Connection db = DriverManager.getConnection(url, username, password);
        PreparedStatement st = db.prepareStatement("DELETE FROM Problems WHERE ID = ?");
        st.setInt(1,Integer.parseInt(aLong.toString()));
        st.executeUpdate();
        st = db.prepareStatement("DELETE FROM studentproblems WHERE problemid = ?");
        st.setInt(1,Integer.parseInt(aLong.toString()));
        st.executeUpdate();
        st.close();
        db.close();
    }
}
