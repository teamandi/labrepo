package ro.ubb.socket.common;

public final class Constants {
    public static final String SERVER_HOST = "192.168.43.135";
    public static final int SERVER_PORT = 1234;

    public enum AvailableMethods{
        getStudents,
        getStudent,
        addStudent,
        deleteStudent,
        updateStudent,

        getProblems,
        getProblem,
        addProblem,
        deleteProblem,
        updateProblem,

        getAssignments,
        assignProblem,
        gradeProblem,
        unassignProblem //opened to new suggestions for this one
    }
}
