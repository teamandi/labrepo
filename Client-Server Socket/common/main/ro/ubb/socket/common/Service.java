package ro.ubb.socket.common;

import ro.ubb.socket.common.domain.Problem;
import ro.ubb.socket.common.domain.Student;

import java.util.concurrent.Future;

public interface Service {
    //these function names should be the same as in Constants.AvailableMethods
    //these functions should generally have the same signature as the methods from StudentService and ProblemService (albeit encapsulated in a Future<>)
    //where return types are void, these functions should return a string with a message (e.g. "Operation successful")

    Future<String> sayHello(String name);

    Future<Message<Iterable<Student>>> getStudents();
    Future<Message<Student>> getStudent(Long id);
    Future<Message<String>> addStudent(Student student);
    Future<Message<String>> deleteStudent(Long id);
    Future<Message<String>> updateStudent(Student student);

    Future<Message<Iterable<Problem>>> getProblems();
    Future<Message<Problem>> getProblem(Long id);
    Future<Message<String>> addProblem(Problem problem);
    Future<Message<String>> deleteProblem(Problem id);
    Future<Message<String>> updateProblem(Problem problem);
}
