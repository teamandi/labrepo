package ro.ubb.socket.common.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * @author teamandi
 *
 */
public class Student extends BaseEntity<Long> implements Serializable{
    private String serialNumber;
    private String name;
    private int group;
    private Map<Long, Float> problems;

    public Student() {
    }

    public Student(String serialNumber, String name, int group) {
        this.serialNumber = serialNumber;
        this.name = name;
        this.group = group;
        this.problems = new HashMap<>();
    }

    public Student(Long id, String serialNumber, String name, int group){
        this.setId(id);
        this.serialNumber = serialNumber;
        this.name = name;
        this.group = group;
        this.problems = new HashMap<>();
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public Set<Long> getProblems() {return problems.keySet();}

    public Optional<Float> getProblem(Long id) {return Optional.ofNullable(problems.get(id));}

    public float removeProblem(Long id){return problems.remove(id);}

    public float getGrade(Long id){
        return problems.get(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (group != student.group) return false;
        if (!serialNumber.equals(student.serialNumber)) return false;
        return name.equals(student.name);

    }

    @Override
    public int hashCode() {
        int result = serialNumber.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + group;
        return result;
    }

    public void addProblem(Long id)throws Exception{
        // check if this problem has already been assigned
        if(problems.containsKey(id))
            throw new Exception("Problem has already been assigned to this student");

        this.problems.put(id, new Float(-1));
    }

    public void addProblem(Long id, float grade){
        this.problems.put(id, new Float(grade));
    }

    public void gradeProblem(Long id, float grade) throws Exception{
        if(!problems.containsKey(id))
            throw new Exception("Invalid problem ID");
        this.problems.put(id, grade);
    }

    @Override
    public String toString() {
        return "Student{" +
                "serialNumber='" + serialNumber + '\'' +
                ", name='" + name + '\'' +
                ", group=" + group +
                "} " + super.toString();
    }
}
