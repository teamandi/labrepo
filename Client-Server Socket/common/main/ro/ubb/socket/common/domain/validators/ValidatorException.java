package ro.ubb.socket.common.domain.validators;

/**
 * @author teamandi
 */

public class ValidatorException extends Exception {
    public ValidatorException(String message) {
        super(message);
    }

    public ValidatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidatorException(Throwable cause) {
        super(cause);
    }
}
