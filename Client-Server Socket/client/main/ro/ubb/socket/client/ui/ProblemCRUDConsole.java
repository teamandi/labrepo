package ro.ubb.socket.client.ui;

import ro.ubb.socket.common.Service;
import ro.ubb.socket.common.domain.Problem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

public class ProblemCRUDConsole {
    private Service problemService;
    private Scanner scanner;
    private ExecutorService executorService;

    public ProblemCRUDConsole(Service problemService, Scanner scanner, ExecutorService executorService) {
        this.problemService = problemService;
        this.scanner = scanner;
        this.executorService = executorService;
    }

    public void runConsole() {
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            System.out.print("Enter a command: ");
            cmd = scanner.next();
            switch (cmd) {
                case "1":
                    addProblem();
                    break;
                case "2":
                    this.executorService.submit(()->printAllProblems());
                    break;
                case "3":
                    this.executorService.submit(()->updateProblem());
                    break;
                case "4":
                    this.executorService.submit(() -> deleteProblem());
                    break;
                case "0":
                    ok=false;
                    break;
                default:
                    break;
            }
        }
    }


    private void printAllProblems() {
        try {

            problemService.getProblems().get().getBody().forEach(System.out::println);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void menu(){
        System.out.println("1. Read a problem.");
        System.out.println("2. Print all problems.");
        System.out.println("3. Update a problem.");
        System.out.println("4. Delete a problem.");
        System.out.println("0. Exit.");
    }

    private void updateProblem(){
        Problem problem = readProblem();
        problemService.updateProblem(problem);
    }

    private void deleteProblem(){
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferRead.readLine());
            Problem p = new Problem("dummy");
            p.setId(id);
            System.out.println(problemService.deleteProblem(p).get().getBody());
        }
        catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
        catch (IOException ex) {
            ex.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void addProblem() {
        Problem problem = readProblem();
        if(problem!=null)
            problemService.addProblem(problem);
    }

    private Problem readProblem() {
        System.out.println("Read problem {id, problemDescription, grade}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferRead.readLine());
            System.out.print("Problem Description = ");
            String problemDescription = bufferRead.readLine();


            Problem problem = new Problem(problemDescription);
            problem.setId(id);

            return problem;
        }
        catch (NumberFormatException nfe){
            System.out.println(nfe.getMessage());
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
