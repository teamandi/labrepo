package ro.ubb.socket.client.ui;

import ro.ubb.socket.common.Message;
import ro.ubb.socket.common.Service;
import ro.ubb.socket.common.domain.Student;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class StudentCRUDConsole {
    private Service studentService;
    private Scanner scanner;
    private ExecutorService executorService;

    public StudentCRUDConsole(Service studentService, Scanner scanner, ExecutorService executorService) {
        this.studentService = studentService;
        this.scanner = scanner;
        this.executorService = executorService;
    }

    public void runConsole() {
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            System.out.print("Enter a command: ");
            cmd = scanner.next();
            switch (cmd) {
                case "1":
                    executorService.submit(()->addStudents());
                    break;
                case "2":
                    executorService.submit(()->printAllStudents());
                    break;
                case "3":
                    executorService.submit(()->updateStudent());
                    break;
                case "4":
                    executorService.submit(()->deleteStudent());
                    break;
                case "0":
                    ok=false;
                    break;
                default:
                    break;
            }
        }
    }

    private static <T> T unpack(Future<Message<T>> msg){
        try {
            return msg.get().getBody();
        } catch (InterruptedException e) {
            System.out.println("An InterruptedException occured");
        } catch (ExecutionException e) {
            System.out.println("An ExecutionException occured");
        }
        return null;
    }

    private void printAllStudents() {
        Iterable<Student> students = unpack(studentService.getStudents());
        if (students == null) return;
        students.forEach(System.out::println);
    }

    private void menu(){
        System.out.println("1. Read a student.");
        System.out.println("2. Print all students.");
        System.out.println("3. Update a student");
        System.out.println("4. Delete a student");
        System.out.println("0. Exit.");
    }

    private void addStudents() {
        Student student = readStudent();
        System.out.println(unpack(studentService.addStudent(student)));
    }

    private void updateStudent(){
        System.out.println("Update student. Please specify the ID of the student you want to update");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferedReader.readLine());
            Student s = unpack(studentService.getStudent(id));
            if (s == null)
                throw new Exception("No student has the given ID");
            System.out.println("Now specify the fields you want to change (leave empty, if you don't want to modify)");

            System.out.print("Serial Number = ");
            s.setSerialNumber(Optional.ofNullable(bufferedReader.readLine()).filter(str->!str.isEmpty()).orElse(s.getSerialNumber()));
            System.out.print("Name = ");
            s.setName(Optional.ofNullable(bufferedReader.readLine()).filter(str->!str.isEmpty()).orElse(s.getName()));
            System.out.print("Group = ");
            String group = bufferedReader.readLine();
            if (!group.equals(""))
                s.setGroup(Integer.parseInt(group));

            //The student is validated inside the update function
            System.out.println(unpack(studentService.updateStudent(s)));
        }
        catch (IOException e) {
            System.out.println("Sorry, the ID should be a number");
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void deleteStudent(){
        System.out.println("Delete student. What is the ID of the student you want to delete?\nID = ");
        Long id = Long.valueOf(scanner.next());
        System.out.println(unpack(studentService.deleteStudent(id)));
    }

    private Student readStudent() {
        System.out.println("Read student {id,serialNumber, name, group}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferRead.readLine());
            System.out.print("Serial Number = ");
            String serialNumber = bufferRead.readLine();
            System.out.print("Name = ");
            String name = bufferRead.readLine();
            System.out.print("Group = ");
            int group = Integer.parseInt(bufferRead.readLine());
            Student student = new Student(serialNumber, name, group);
            student.setId(id);

            return student;
        }
        catch (NumberFormatException nfe){
            System.out.println(nfe.getMessage());
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
