package ro.ubb.socket.client.ui;

import ro.ubb.socket.common.Message;
import ro.ubb.socket.common.Service;
import ro.ubb.socket.common.domain.Problem;
import ro.ubb.socket.common.domain.Student;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class AssignConsole {
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RESET = "\u001B[0m";
    private Service service;
    private Scanner scanner;
    private ExecutorService executorService;

    public AssignConsole(Service service, Scanner scanner, ExecutorService executorService) {
        this.service = service;
        this.scanner = scanner;
        this.executorService = executorService;
    }

    public void runConsole(){
        String cmd;
        boolean ok=true;
        while(ok) {
            menu();
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter a command: ");
            try {
                cmd = bufferRead.readLine();
                switch (cmd) {
                    case "1":
                        executorService.submit(()->printAll());
                        break;
                    case "2":
                        executorService.submit(()->assignProblem());
                        break;
                    case "3":
                        executorService.submit(()->gradeStudent());
                        break;
                    case "4":
                        executorService.submit(()->deleteProblem());
                        break;
                    case "0":
                        ok = false;
                        break;
                    default:
                        System.out.println("Invalid command. Please try again");
                        break;
                }
            }
            catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
    }

    private void menu(){
        System.out.println("1. See students and assigned problems");
        System.out.println("2. Assign a problem to a student");
        System.out.println("3. Grade a student");
        System.out.println("4. Remove a problem from a student");
        System.out.println("0. Go back");
    }

    private static <T> T unpack(Future<Message<T>> msg){
        try {
            return msg.get().getBody();
        } catch (InterruptedException e) {
            System.out.println("An InterruptedException occured");
        } catch (ExecutionException e) {
            System.out.println("An ExecutionException occured");
        }
        return null;
    }

    private void printAll() {
        unpack(service.getStudents())
                .forEach(
                        s->{
                            System.out.println(ANSI_GREEN + "<" + s + "> has assigned the following problems:" + ANSI_RESET);
                            s.getProblems().stream()
                                    .forEach(p->System.out.println(ANSI_RED + "\t" + unpack(service.getProblem(p)) + ANSI_RESET));
                        }
                );
    }

    private void assignProblem() {
        System.out.println("Please provide the ID of the student you want to assign problems to");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferedReader.readLine());
            Student s = unpack(service.getStudent(id));
            if (s == null)
                new Exception("No student has that ID");
            System.out.println("Now provide the ID of the problem you want to assign to <" + s + ">");
            System.out.print("ID = ");
            id = Long.valueOf(bufferedReader.readLine());
            Problem p = unpack(service.getProblem(id));
            if (p == null)
                new Exception("No problem has that ID");
            s.addProblem(id);
            service.updateStudent(s);
            System.out.println(ANSI_GREEN + p
                    + ANSI_RED + " -- successfully assigned to -- "
                    + ANSI_GREEN + s + ANSI_RESET);
        }
        catch (IOException e){
            System.out.println("Sorry, ID should be a number");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void gradeStudent() {
        System.out.println("Please provide the ID of the student you want to grade");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferedReader.readLine());
            Student s = unpack(service.getStudent(id));
            if (s == null)
                new Exception("No student has that ID");
            System.out.println("Now provide the ID of the problem you want to grade");
            id = Long.valueOf(bufferedReader.readLine());
            s.getProblem(id).orElseThrow(() -> new Exception("Student has not been assigned that problem"));
            System.out.println("What grade would you like to assign to this problem?");
            float grade = Float.valueOf(bufferedReader.readLine());
            s.gradeProblem(id, grade);
            service.updateStudent(s);
        }
        catch (IOException e){
            System.out.println("Sorry, ID should be a number");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void deleteProblem(){
        System.out.println("Please provide the ID of the student you want to delete a problem from");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("ID = ");
            Long id = Long.valueOf(bufferedReader.readLine());
            Student s = unpack(service.getStudent(id));
            if (s == null)
                new Exception("No student has that ID");
            System.out.println("Now provide the ID of the problem you want to remove");
            id = Long.valueOf(bufferedReader.readLine());
            s.getProblem(id).orElseThrow(() -> new Exception("Student has not been assigned that problem"));
            s.removeProblem(id);
            service.updateStudent(s);
            System.out.println(ANSI_RED + "Problem successfully removed" + ANSI_RESET);
        }
        catch (IOException e){
            System.out.println("Sorry, ID should be a number");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
