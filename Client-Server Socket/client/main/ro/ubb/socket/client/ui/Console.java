package ro.ubb.socket.client.ui;

import ro.ubb.socket.common.Service;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;

public class Console {
    private Service service;
    private Scanner scanner;
    private ExecutorService executorService;

    public Console(Service service, ExecutorService executorService) {
        this.service = service;
        this.scanner = new Scanner(System.in);
        this.executorService = executorService;
    }
    public void runConsole() {
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            System.out.print("Enter a command: ");
            cmd = scanner.next();
            switch (cmd) {
                case "1":
                    StudentCRUDConsole studentCRUDConsole = new StudentCRUDConsole(service, scanner, executorService);
                    studentCRUDConsole.runConsole();
                    break;
                case "2":
                    ProblemCRUDConsole problemCRUDConsole = new ProblemCRUDConsole(service, scanner, executorService);
                    problemCRUDConsole.runConsole();
                    break;
                case "3":
                    AssignConsole assigningConsole = new AssignConsole(service, scanner, executorService);
                    assigningConsole.runConsole();
                    break;
                case "0":
                    scanner.close();
                    ok=false;
                default:
                    break;
            }
        }
    }

    private void menu(){
        System.out.println("1. Student CRUD.");
        System.out.println("2. Problems CRUD");
        System.out.println("3. Assign Console");
        System.out.println("0. Exit.");
    }
}
