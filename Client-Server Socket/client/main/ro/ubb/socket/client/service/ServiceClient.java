package ro.ubb.socket.client.service;

import ro.ubb.socket.client.tcp.TcpClient;
import ro.ubb.socket.common.Constants;
import ro.ubb.socket.common.Message;
import ro.ubb.socket.common.Service;
import ro.ubb.socket.common.domain.Problem;
import ro.ubb.socket.common.domain.Student;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class ServiceClient implements Service {
    private ExecutorService executorService;
    private TcpClient tcpClient;

    public ServiceClient(ExecutorService executorService, TcpClient tcpClient) {
        this.executorService = executorService;
        this.tcpClient = tcpClient;
    }

    @Override
    public Future<String> sayHello(String name) {
        return null;
    }

    @Override
    public Future<Message<Iterable<Student>>> getStudents() {
        return executorService.submit(()->{
            Message<Void> request = new Message<>(Constants.AvailableMethods.getStudents.name());
            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<Student>> getStudent(Long id) {
        return executorService.submit(()->{
            Message<Long> request = new Message<>(Constants.AvailableMethods.getStudent.name(), id);
            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<String>> addStudent(Student student) {
        return executorService.submit(() -> {
            Message<Student> request = new Message<>(Constants.AvailableMethods.addStudent.name(), student);
            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<String>> deleteStudent(Long id) {
        return executorService.submit(() -> {
            Message<Long> request = new Message<>(Constants.AvailableMethods.deleteStudent.name(), id);
            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<String>> updateStudent(Student student) {
        return executorService.submit(() -> {
            Message<Student> request = new Message<>(Constants.AvailableMethods.updateStudent.name(), student);
            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<Iterable<Problem>>> getProblems() {
        return executorService.submit(()->{
            Message<Void> request = new Message<>(Constants.AvailableMethods.getProblems.name());

            return tcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<Problem>> getProblem(Long id) {
        return executorService.submit(()->{
            Message<Long> request = new Message<>(Constants.AvailableMethods.getProblem.name(), id);

            return tcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<String>> addProblem(Problem problem) {
        return executorService.submit(()->{
            Message<Problem> request = new Message<>(Constants.AvailableMethods.addProblem.name(),problem);

            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<String>> deleteProblem(Problem p) {
        return executorService.submit(()->{

            Message<Problem> request = new Message<>(Constants.AvailableMethods.deleteProblem.name(),p);

            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<String>> updateProblem(Problem problem) {
        return executorService.submit(()->{
            Message<Problem> request = new Message<>(Constants.AvailableMethods.updateProblem.name(),problem);

            return TcpClient.sendAndReceive(request);
        });
    }
}
