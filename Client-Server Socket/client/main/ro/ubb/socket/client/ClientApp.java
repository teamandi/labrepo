package ro.ubb.socket.client;

import ro.ubb.socket.client.service.ServiceClient;
import ro.ubb.socket.client.tcp.TcpClient;
import ro.ubb.socket.client.ui.Console;
import ro.ubb.socket.common.Constants;
import ro.ubb.socket.common.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ClientApp {
    public static void main(String[] args){
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        TcpClient tcpClient = new TcpClient(Constants.SERVER_HOST,Constants.SERVER_PORT);
        Service serviceClient = new ServiceClient(executorService,tcpClient);

        Console console = new Console(serviceClient, executorService);
        console.runConsole();

        executorService.shutdownNow();
        System.out.println("Bye client!");
    }
}
