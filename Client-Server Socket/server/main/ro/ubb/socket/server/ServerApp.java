package ro.ubb.socket.server;

import ro.ubb.socket.common.Constants;
import ro.ubb.socket.common.Message;
import ro.ubb.socket.common.Service;
import ro.ubb.socket.common.domain.Problem;
import ro.ubb.socket.common.domain.Student;
import ro.ubb.socket.common.domain.validators.ProblemValidator;
import ro.ubb.socket.common.domain.validators.StudentValidator;
import ro.ubb.socket.server.repository.Repository;
import ro.ubb.socket.server.repository.SQL.ProblemSQLRepo;
import ro.ubb.socket.server.repository.SQL.StudentSQLRepo;
import ro.ubb.socket.server.service.ServiceImpl;
import ro.ubb.socket.server.tcp.TcpServer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.UnaryOperator;

public class ServerApp {
    public static void main(String[] args){

        ProblemValidator problemValidator = new ProblemValidator();
        StudentValidator studentValidator = new StudentValidator();
        String url = "jdbc:postgresql://baasu.db.elephantsql.com:5432/pkzccpxq";
        String username = "pkzccpxq";
        String password = "B357W-Hxfjabf4gquK_ZbfPBVZvD0MLk";
        Repository<Long, Problem> problemRepository = new ProblemSQLRepo(problemValidator, url, username, password);
        Repository<Long, Student> studentRepository = new StudentSQLRepo(studentValidator, url, username, password);

        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        TcpServer tcpServer = new TcpServer(executorService, Constants.SERVER_PORT);
        Service service = new ServiceImpl(executorService, problemRepository, studentRepository);

        //tcpServer.addHandler(Constants.AvailableMethods.addProblem.name(), (request)->{
        //    Future<String> res = service.addProblem(request.getBody());
        //});
        handlers(tcpServer,service);
        /*TEMPLATE

         */

        tcpServer.startServer();
        System.out.println("bye - server");
    }

    public static UnaryOperator<Message> makeHandler(Message msg){
        for(Method m : Service.class.getDeclaredMethods())
            if(m.getName().equals(msg.getHeader()))
            {
                UnaryOperator<Message> handle = (request) -> {
                    try {
                        Future res = (Future<Message>) m.invoke(Service.class, msg.getBody());
                        return (Message)res.get();
                    } catch (IllegalAccessException | ExecutionException | InterruptedException | InvocationTargetException e) {
                        System.out.println(e.getMessage());
                    }
                    return null;
                };
                return handle;
            }
        return null;
    }

    public static void handlers(TcpServer tcpServer, Service service){
        tcpServer.addHandler(Constants.AvailableMethods.addStudent.name(), (request) -> {
            Student s = (Student)request.getBody();
            Future res = service.addStudent(s);
            try {
                return (Message<String>)res.get();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            } catch (ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.getStudents.name(), (request) -> {
            Future res = service.getStudents();
            try {
                return (Message<Iterable<Student>>)res.get();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            } catch (ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.getStudent.name(), (request) -> {
            Long id = (Long)request.getBody();
            Future res = service.getStudent(id);
            try{
                return (Message<Student>)res.get();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            } catch (ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.deleteStudent.name(), (request) -> {
            Long id = (Long)request.getBody();
            Future res = service.deleteStudent(id);
            try{
                return (Message<String>)res.get();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            } catch (ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.updateStudent.name(), (request) -> {
            Student s = (Student)request.getBody();
            Future res = service.updateStudent(s);
            try {
                return (Message<String>)res.get();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            } catch (ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.addProblem.name(), (request) -> {
            Problem pb = (Problem)request.getBody();
            Future res = service.addProblem(pb);
            try {
                return (Message<String>)res.get();
                //return new Message<Optional<Problem>>(request.getHeader(), res.get());
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.getProblems.name(),(request)->{
            Future res = service.getProblems();
            try {
                return (Message<String>) res.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.getProblem.name(), (request) -> {
            Long id = (Long)request.getBody();
            Future res = service.getProblem(id);
            try {
                return (Message<Problem>)res.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.deleteProblem.name(),(request)->{
            Problem pb = (Problem)request.getBody();
            Future res = service.deleteProblem(pb);
            try {
                return (Message<String>)res.get();
                //return new Message<Optional<Problem>>(request.getHeader(), res.get());
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.updateProblem.name(),(request)->{
            Problem pb = (Problem)request.getBody();
            Future res = service.updateProblem(pb);

            try {
                return (Message<String>)res.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        });

    }
}
