package ro.ubb.socket.server.service;

import ro.ubb.socket.common.Constants;
import ro.ubb.socket.common.Message;
import ro.ubb.socket.common.Service;
import ro.ubb.socket.common.domain.Problem;
import ro.ubb.socket.common.domain.Student;
import ro.ubb.socket.common.domain.validators.ValidatorException;
import ro.ubb.socket.server.repository.Repository;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class ServiceImpl implements Service{
    private ExecutorService executorService;
    private Repository<Long, Problem> problemRepository;
    private Repository<Long, Student> studentRepository;

    public ServiceImpl(ExecutorService executorService, Repository<Long, Problem> problemRepository, Repository<Long, Student> studentRepository) {
        this.executorService = executorService;
        this.problemRepository = problemRepository;
        this.studentRepository = studentRepository;
    }

    public ServiceImpl(ExecutorService executorService) {
        this.executorService = executorService;
    }

    @Override
    public Future<String> sayHello(String name) {
        return executorService.submit(()->"Hello" + name);
    }

    @Override
    public Future<Message<Iterable<Student>>> getStudents() {
        return executorService.submit(() -> {
            return new Message<>(Constants.AvailableMethods.getStudents.name(), studentRepository.findAll());
        });
    }

    @Override
    public Future<Message<Student>> getStudent(Long id) {
        return executorService.submit(() -> {
            return new Message<>(Constants.AvailableMethods.getStudent.name(), studentRepository.findOne(id).orElse(null));
        });
    }

    @Override
    public Future<Message<String>> addStudent(Student student) {
        return executorService.submit(() -> {
            String msg;
            try {
                studentRepository.save(student).get();
                msg = "There already exists a student with the same ID";
            }
            catch (ValidatorException e) {
                msg = e.getMessage();
            }
            catch (NoSuchElementException e){
                msg = "Student added successfully";
            }
            return new Message<String>(Constants.AvailableMethods.addStudent.name(), msg);
        });
    }

    @Override
    public Future<Message<String>> deleteStudent(Long id) {
        return executorService.submit(() -> {
            String msg;
            try {
                studentRepository.delete(id).get();
                msg = "Student removed successfully";
            }
            catch (NoSuchElementException e){
                msg = "No student has the given ID";
            }
            return new Message<String>(Constants.AvailableMethods.deleteStudent.name(), msg);
        });
    }

    @Override
    public Future<Message<String>> updateStudent(Student student) {
        return executorService.submit(() -> {
            String msg;
            try {
                studentRepository.update(student).get();
                msg = "No student has the given ID";
            }
            catch (NoSuchElementException e){
                msg = "Student updated successfully";
            }
            return new Message<String>(Constants.AvailableMethods.updateProblem.name(), msg);
        });
    }

    @Override
    public Future<Message<Iterable<Problem>>> getProblems() {
        return executorService.submit(()->{
            return new Message<>(Constants.AvailableMethods.getProblems.name(), problemRepository.findAll());
        });
    }

    @Override
    public Future<Message<Problem>> getProblem(Long id) {
        return executorService.submit(()-> {
            return new Message<>(Constants.AvailableMethods.getProblem.name(), problemRepository.findOne(id).orElse(null));
        });
    }

    @Override
    public Future<Message<String>> addProblem(Problem problem) {
        return executorService.submit(()->{
            try {
                Message<String> response = new Message<>(Constants.AvailableMethods.addProblem.name(),"Can't add.");
                if(problemRepository.save(problem).equals(Optional.empty())) {
                    response = new Message<>(Constants.AvailableMethods.addProblem.name(),"Successfully added.");
                    return response;
                }
                return response;
            } catch (ValidatorException e) {
                e.printStackTrace();
            }
            return null;
        });
    }

    @Override
    public Future<Message<String>> deleteProblem(Problem p) {
        return executorService.submit(()->{
            Message<String> response = new Message<>(Constants.AvailableMethods.deleteProblem.name(),"Can't remove.");
            if(problemRepository.delete(p.getId()).equals(Optional.empty())) {
                response = new Message<>(Constants.AvailableMethods.deleteProblem.name(),"Successfully removed.");
                return response;
            }
            return response;
        });
    }

    @Override
    public Future<Message<String>> updateProblem(Problem problem) {
        return executorService.submit(()->{
            try {
                Message<String> response = new Message<>(Constants.AvailableMethods.updateProblem.name(),"Can't update.");
                if(problemRepository.update(problem).equals(Optional.empty())) {
                    response = new Message<>(Constants.AvailableMethods.updateProblem.name(),"Successfully updated.");
                    return response;
                }
                return response;
            } catch (ValidatorException e) {
                e.printStackTrace();
            }
            return null;
        });
    }
}
